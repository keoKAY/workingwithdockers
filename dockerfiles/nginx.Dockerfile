FROM nginx:latest
ARG NGINX_NUM=01
# COPY ./nginx/index.html /usr/share/nginx/html/index.html

ADD ./resources.tar /
ADD https://i.pinimg.com/736x/c4/73/a2/c473a2c0f1e9d6348c33bd660f1c1888.jpg /
RUN apt update && apt install git -y
RUN  sed -i "s/Welcome to nginx/Welcome to Nginx ${NGINX_NUM}/g" /usr/share/nginx/html/index.html 
EXPOSE 80

# docker build -t another-nginx-custom-2 -f nginx.Dockerfile --build-arg NGINX_NUM=02   .
# docker run -d -p 8088:80 --name another-cont another-nginx-custom-2